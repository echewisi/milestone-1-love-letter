// Wir importieren die benötigten Klassen
import java.util.ArrayList;
import java.util.Scanner;

// Wir definieren unsere Hauptklasse
public class LoveLetterSpiel {
    public static void main(String[] args) {
        // Wir erstellen einen Scanner, um Benutzereingaben zu lesen
        Scanner scanner = new Scanner(System.in);

        // Wir erstellen Listen, um die Spieler und ihre Karten zu speichern
        ArrayList<String> spieler = new ArrayList<>();
        ArrayList<String> spielernamen = new ArrayList<>();
        ArrayList<String> spielerkarten = new ArrayList<>();

        // Wir fragen nach der Anzahl der Spieler
        System.out.print("Geben Sie die Anzahl der Spieler ein (2-4): ");
        int spieleranzahl = scanner.nextInt();
        scanner.nextLine();  // Umbruch einlesen

        // Wir fragen nach den Namen der Spieler
        for (int i = 0; i < spieleranzahl; i++) {
            System.out.print("Geben Sie den Namen von Spieler " + (i + 1) + " ein: ");
            String spielername = scanner.nextLine();
            spieler.add(spielername);
            spielernamen.add(spielername);
            spielerkarten.add("");  // Jeder Spieler startet ohne Karte
        }

        // Wir starten das Spiel
        System.out.println("Spiel gestartet!");

        // Hauptspiel-Schleife
        while (true) {
            // Wir lesen den nächsten Befehl ein
            System.out.print("Geben Sie einen Befehl ein (geben Sie '\\help' ein, um eine Liste der Befehle anzuzeigen): ");
            String befehl = scanner.nextLine();

            // Wir verarbeiten den eingegebenen Befehl
            if (befehl.equals("\\help")) {
                // Der Benutzer hat nach Hilfe gefragt, also zeigen wir die verfügbaren Befehle an
                System.out.println("Verfügbare Befehle:");
                System.out.println("\\start - Startet das Spiel.");
                System.out.println("\\playCard - Spielt eine Karte.");
                System.out.println("\\showHand - Zeigt die Hand des aktuellen Spielers.");
                System.out.println("\\showScore - Zeigt den Punktestand der Spieler.");
                System.out.println("\\exit - Beendet das Spiel.");
            } else if (befehl.equals("\\start")) {
                // Der Benutzer möchte das Spiel starten, also tun wir das
                System.out.println("Spiel gestartet.");
            } else if (befehl.equals("\\playCard")) {
                // Der Benutzer möchte eine Karte spielen, also fragen wir nach seinem Namen und der Karte, die er spielen möchte
                System.out.print("Geben Sie Ihren Namen ein: ");
                String spielername = scanner.nextLine();

                if (spielernamen.contains(spielername)) {
                    System.out.print("Geben Sie den Kartennamen ein: ");
                    String kartename = scanner.nextLine();

                    int spielerindex = spielernamen.indexOf(spielername);
                    spielerkarten.set(spielerindex, kartename);

                    System.out.println("Karte gespielt!");
                } else {
                    System.out.println("Spieler nicht gefunden.");
                }
            } else if (befehl.equals("\\showHand")) {
                // Der Benutzer möchte seine Hand anzeigen, also tun wir das
                System.out.print("Geben Sie Ihren Namen ein: ");
                String spielername = scanner.nextLine();

                if (spielernamen.contains(spielername)) {
                    int spielerindex = spielernamen.indexOf(spielername);
                    String hand = spielerkarten.get(spielerindex);
                    System.out.println("Hand von " + spielername + ": " + hand);
                } else {
                    System.out.println("Spieler nicht gefunden.");
                }
            } else if (befehl.equals("\\showScore")) {
                // Der Benutzer möchte den Punktestand anzeigen, also tun wir das
                for (int i = 0; i < spieleranzahl; i++) {
                    System.out.println("Punktzahl von " + spielernamen.get(i) + ": " + spielerkarten.get(i).length());
                }
            } else if (befehl.equals("\\exit")) {
                // Der Benutzer möchte das Spiel beenden, also tun wir das und beenden die Schleife
                System.out.println("Auf Wiedersehen!");
                break;
            } else {
                // Wenn wir hier ankommen, hat der Benutzer einen Befehl eingegeben, den wir nicht erkennen
                System.out.println("Unbekannter Befehl. Geben Sie '\\help' ein, um eine Liste der Befehle anzuzeigen.");
            }
        }
    }
}
